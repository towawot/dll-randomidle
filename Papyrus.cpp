#include <skse.h>
#include <skse/GameCamera.h>
#include <skse/PapyrusNativeFunctions.h>
#include <skse/GameRTTI.h>
#include <skse/GameData.h>

#define _USE_MATH_DEFINES
#include <math.h>

#include "Papyrus.h"
#include "Address.h"

#include <set>
#include <string>
#include <algorithm>
std::set<std::string> g_AnimEvent;
std::set<std::string> g_BypassEvent;

bool bAllowBypass;

bool AllowedSendEvent(std::string str)
{
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	if (g_AnimEvent.find(str) != g_AnimEvent.end())
	{
		return true;
	}
	return false;
}

bool IsRandomIdleEvent_Base(std::string str)
{
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	std::string::size_type index = str.find("randomidle");
	if (index != std::string::npos)
	{
		return true;
	}
	return false;
}
	
bool SendBypassEvent(std::string str)
{
	if (bAllowBypass)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		if (g_BypassEvent.find(str) != g_BypassEvent.end())
		{
			return true;
		}
	}
	return false;
}

const char* GetFullName(TESObjectREFR* ref)
{
	static const char unkName[] = "unknown";
	const char* result = unkName;
	TESFullName* pFullName = NULL;

	if (ref && ref->formType == kFormType_Character)
	{
		TESActorBase* actorBase = (TESActorBase*)ref->baseForm;
		if (actorBase)	pFullName = DYNAMIC_CAST(actorBase, TESActorBase, TESFullName);
	}
	else if (ref && ref->formType == kFormType_Reference)
	{
		TESForm* form = (TESForm*)ref->baseForm;
		if(form)	pFullName = DYNAMIC_CAST(form, TESForm, TESFullName);
	}

	if (pFullName)	result = pFullName->name.data;

	return result;
}

bool IsPlayerTeammate(Actor* actor)
{
	if (actor)
	{
		return (actor->flags1 & actor->kFlags_IsPlayerTeammate) != 0;
	}
	return false;
}


// パピルス関数本体
namespace Papyrus
{
	static const char s_className[] ="towRandomIdle";

	// AnimEventの登録・解除
	void RegisterForAnimEvent_base(StaticFunctionTag*, BSFixedString eventName, UInt32 mode)
	{
		if (eventName.data != NULL && eventName.data[0] != 0)
		{
			std::string str = eventName.data;
			std::transform(str.begin(), str.end(), str.begin(), ::tolower);
			switch(mode)
			{
			case 0:
				g_AnimEvent.erase(str);
				break;
			case 1:
				g_AnimEvent.insert(str);
				break;
			case 2:
				g_BypassEvent.insert(str);
				break;
			}
		}
	}
	
	bool IsRandomIdleEvent(StaticFunctionTag*, BSFixedString eventName)
	{
		if (eventName.data != NULL && eventName.data[0] != 0)
		{
			std::string str = eventName.data;
			return IsRandomIdleEvent_Base(str);
		}
		return false;
	}
	
	void SwitchBypassEvent(StaticFunctionTag*, bool bSendEvent)
	{
		bAllowBypass = bSendEvent;
	}
	
	void RegisterFuncs(VMClassRegistry* registry)
	{
	registry->RegisterFunction(
		new NativeFunction2<StaticFunctionTag, void, BSFixedString, UInt32>("RegisterForAnimEvent_base", s_className, RegisterForAnimEvent_base, registry));
	registry->RegisterFunction(
		new NativeFunction1<StaticFunctionTag, bool, BSFixedString>("IsRandomIdleEvent", s_className, IsRandomIdleEvent, registry));
	registry->RegisterFunction(
		new NativeFunction1<StaticFunctionTag, void, bool>("SwitchBypassEvent", s_className, SwitchBypassEvent, registry));
	}
}
