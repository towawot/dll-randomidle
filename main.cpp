#include <skse.h>
#include <skse/skse_version.h>
#include <skse/GameData.h>
#include <skse/GamePapyrus.h>
#include <skse/PluginAPI.h>
#include <skse/SafeWrite.h>
#include <shlobj.h>
// #include <skse/GameExtraData.h>
// #include <skse/GameRTTI.h>

#include <set>
#include <string>
#include <algorithm>

#include "Papyrus.h"
#include "Address.h"

#ifdef _DEBUG
	IDebugLog gLog;
#endif
PluginHandle g_pluginHandle = kPluginHandle_Invalid;


// OnAnimationEventを事前に監視する
// static UInt32 fnIAnimationGraphManagerHolder_Unk_01;
static UInt32 fnIPlayerAnimationGraphManagerHolder_Unk_01;
static bool Log_IAnimationGraphManagerHolder_SendEvent(UInt32* stack, UInt32 ecx)
{
	BSFixedString* animName = (BSFixedString*)stack[1];
	TESObjectREFR* ref = (TESObjectREFR*)(ecx - 0x20);
	PlayerCharacter* player = *g_thePlayer;

	if(!ref)	return kEvent_Continue;
	if (ref->formType == kFormType_Character && (ref == player || IsPlayerTeammate((Actor*)ref)))
	{
		std::string str = animName->data;
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);

		#ifdef _DEBUG
			const char* name = GetFullName(ref);
			_MESSAGE("%s by %s(%08X)", animName->data, name, ref);
		#endif
		
		if (SendBypassEvent(str))
		{
			static BSFixedString eventName("OnBypassEvent");
			PapyrusCall::SendEvent(ref, &eventName, ref, animName->data);
			return false;
		}
		
		if (AllowedSendEvent(str) || IsRandomIdleEvent_Base(str))
		{
			static BSFixedString eventName("OnAnimationEventEX");
			PapyrusCall::SendEvent(ref, &eventName, ref, animName->data);
		}
	}
	return true;
}

static void __declspec(naked) Hook_IPlayerAnimationGraphManagerHolder_Unk_01(void)
{
	__asm
	{
		push	ecx
		lea		ecx, [esp+4]
		push	ecx
		call	Log_IAnimationGraphManagerHolder_SendEvent
		add		esp, 4
		pop		ecx
		
		test	al, al
		jnz		jmp_default

		retn	4
jmp_default:
		jmp		fnIPlayerAnimationGraphManagerHolder_Unk_01
	}
}

// OnActivateイベントハンドラ
DECLARE_EVENT_HANDLER(Activate, ActivateEventHandler);
EventResult ActivateEventHandler::ReceiveEvent(TESActivateEvent *evn, DispatcherT *dispatcher)
{
	if(!evn->caster)	return kEvent_Continue;
	if(!evn->target)	return kEvent_Continue;
	PlayerCharacter* player = *g_thePlayer;

	if (evn->caster == player || IsPlayerTeammate((Actor*)evn->caster))
	{
		TESForm* form = (evn->target)->baseForm;
		if (form && form->formType == kFormType_Furniture)
		{
			#ifdef _DEBUG
				_MESSAGE("activate: %s by %s(%08X)", GetFullName(evn->target), GetFullName(evn->caster), evn->caster);
			#endif

			static BSFixedString eventName("OnActorFurnitureActivate");
			PapyrusCall::SendEvent(evn->caster, &eventName, evn->target, evn->caster);
		}
	}

	return kEvent_Continue;
}

// OnMagicEffectApplyを拾うイベントハンドラ
DECLARE_EVENT_HANDLER(MagicEffectApply, MagicEffectApplyEventHandler);
EventResult MagicEffectApplyEventHandler::ReceiveEvent(TESMagicEffectApplyEvent *evn, DispatcherT *dispatcher)
{
	Unregister();
	DataHandler* dhnd = DataHandler::GetSingleton();
	if (dhnd->LookupModByName("towRandomIdle.esp") != NULL)
	{
		#ifdef _DEBUG
			_MESSAGE("register papyrus function");
		#endif
		Papyrus::RegisterFuncs((*g_skyrimVM)->GetClassRegistry()); 
	}
	else
	{
		#ifdef _DEBUG
			_MESSAGE("warning: %s is not active.", "towRandomIdle.esp");
		#endif
	}

// 	activateイベントを拾う
	static ActivateEventHandler s_handlerActivate;
	s_handlerActivate.Register();

// プレイヤーのAnimationGraphManagerHolderにフックをかける
	UInt32* vptr = (UInt32*)ADDR_IPlayerAnimationGraphManagerHolder_VPTR;
	fnIPlayerAnimationGraphManagerHolder_Unk_01 = vptr[0x01];
	SafeWrite32((UInt32)&vptr[0x01], (UInt32)&Hook_IPlayerAnimationGraphManagerHolder_Unk_01);
// 	vptr = (UInt32*)ADDR_IAnimationGraphManagerHolder_VPTR;
// 	fnIAnimationGraphManagerHolder_Unk_01 = vptr[0x01];
// 	SafeWrite32((UInt32)&vptr[0x01], (UInt32)&Hook_IAnimationGraphManagerHolder_Unk_01);
	return kEvent_Continue;
}

extern "C"
{

bool SKSEPlugin_Query(const SKSEInterface * skse, PluginInfo * info)
{
#ifdef _DEBUG
	gLog.OpenRelative(CSIDL_MYDOCUMENTS, "\\My Games\\Skyrim\\SKSE\\skse_towRandomIdle.log");
	_MESSAGE("towRandomIdle.dll v%s", "1.0");
#endif

	// populate info structure
	info->infoVersion =	PluginInfo::kInfoVersion;
	info->name =		"towRandomIdle DLL";
	info->version =		1.0;


	// store plugin handle so we can identify ourselves later
	g_pluginHandle = skse->GetPluginHandle();
	
	if(skse->isEditor)
	{
		#ifdef _DEBUG
			_MESSAGE("loaded in editor, marking as incompatible");
		#endif
		return false;
	}

	int major = (skse->runtimeVersion >> 24) & 0xFF;
	int minor = (skse->runtimeVersion >> 16) & 0xFF;
	int build = (skse->runtimeVersion >> 8) & 0xFF;
	int sub   = skse->runtimeVersion & 0xFF;

	if(skse->runtimeVersion != SKSE_SUPPORTING_RUNTIME_VERSION)
	{
		#ifdef _DEBUG
			_MESSAGE("unsupported runtime version 1.%d.%d.%d", major, minor, build);
		#endif

		major = (SKSE_SUPPORTING_RUNTIME_VERSION >> 24) & 0xFF;
		minor = (SKSE_SUPPORTING_RUNTIME_VERSION >> 16) & 0xFF;
		build = (SKSE_SUPPORTING_RUNTIME_VERSION >> 8) & 0xFF;
		sub   = SKSE_SUPPORTING_RUNTIME_VERSION & 0xFF;
		#ifdef _DEBUG
			_MESSAGE("(this plugin needs 1.%d.%d.%d)", major, minor, build);
		#endif
		return false;
	}

	#ifdef _DEBUG
		_MESSAGE("skyrim runtime version: 1.%d.%d.%d.%d ... OK !", major, minor, build);
	#endif

	return true;
}


bool SKSEPlugin_Load(const SKSEInterface * skse)
{
	#ifdef _DEBUG
		_MESSAGE("loaded (handle:%08X)", g_pluginHandle);
	#endif

	static MagicEffectApplyEventHandler s_handlerMagicEffect;
	s_handlerMagicEffect.Register();

	return true;
}

}

