#pragma once

class VMClassRegistry;
struct StaticFunctionTag;

bool AllowedSendEvent(std::string str);
bool SendBypassEvent(std::string str);
const char* GetFullName(TESObjectREFR* ref);
bool IsPlayerTeammate(Actor* actor);
bool IsRandomIdleEvent_Base(std::string str);

namespace Papyrus
{
	void RegisterForAnimEvent_base(StaticFunctionTag*, BSFixedString eventName, UInt32 mode);
	void SwitchBypassEvent(StaticFunctionTag*, bool bSendEvent);

	void RegisterFuncs(VMClassRegistry* registry);
}
